﻿% on debain install the follwoing packages for apacite and IEEEtrans respectively
% sudo apt-get install  texlive-publishers
% sudo apt-get install texlive-bibtex-extra
\title{Commercial Rent Re-Negotiation of an existing lease \\
  \large``It never hurts to ask''\\
  \large MBAX 6531-572 : negotiations }
\author{
Chris John \\
Sam Povilus \\ 
Tim Schuermer \\
Josh Hansen \\
John Regan \\
}
\date{\today}

\documentclass[12pt]{article}

\usepackage[letterpaper, margin=1.0in]{geometry}
\usepackage{setspace}
\doublespacing
\usepackage{apacite}

\usepackage[hyphens]{url}
\usepackage{hyperref}
\hypersetup{breaklinks=true}

\urlstyle{same}

\begin{document}
\clearpage\maketitle
\thispagestyle{empty}
\newpage
\pagenumbering{roman}
\tableofcontents
\newpage
\pagenumbering{arabic}
\section{Executive summary}

Note: This negotiation involves individuals and businesses in the local Boulder community. Because of this, names have been changed or omitted to maintain their privacy.


In 2008 a tenant signed a new five-year lease to start Boulder Bagels. She was an inexperienced business person and unaware she had the ability to negotiate for better rental terms than what were offered. Six months into her lease and struggling to turn a profit, she discovered not only was her rental rate unusually high for the area but that she did not receive any market-standard financial incentives offered by landlords when initiating a new lease. With this knowledge she critically assessed her financial situation, the growth rate of her business, and various foreseeable repairs she expected to make in the next year. She decided to approach her landlord seeking a permanent rent reduction and forgiveness of two months of rental payments in order to be on more stable financial footing. Questioned why she thought it was reasonable to approach the landlord after signing the lease, she stated “it never hurts to ask”.


The landlord representing a small group of investors was sympathetic to the issues raised by the tenant but unwilling to reduce rent. He made good faith efforts to understand the concerns raised and worked to find a solution which made both parties satisfied while not giving ground on the primary request of reduced rent.


The analysis of the negotiation shows the tenant entered the negotiation with a distributive perspective, looking to reduce the total amount of money paid to the landlord over the lifetime of the lease. Given the relative power difference between tenant and landlord, the tenant relied on a plea to fairness, and floating the possibility that in the event of bankruptcy she would be unable to pay any rent at all. The landlord worked to make his counter proposals integrative, such that both parties benefited from any monetary outlay he agreed to. Additionally, while the landlord could have negotiated from a power position, using the signed lease as leverage to stop negotiations, he instead chose to negotiate on issues and identify a mutually satisfactory agreement. The outcome satisfied both parties and established a successful working relationship for the remainder of the lease. 


The initial request made by the tenant would cost the landlord \$64,000
\begin{itemize}
\item Reduction of rent by \$1,000/mo (\$54,000)
\item No payment on two-months rent (\$10,000)
\end{itemize}

The final decision agreed to by both parties cost the landlord ~\$16,000
\begin{itemize}
\item 2 months deferred rent payment, repayable at the conclusion of the lease
\item Cover the cost for flyers to advertise (\$8000)
\item Replace exterior awnings and other repairs (\$8000)
\item Lease was renewed for 5 full years
\end{itemize}

\section{Background}

The negotiation started when a small restaurant owner for Boulder Bagels, who was six months into a five-year lease, discovered a number of factors that made her believe her lease with the landlord (“Colorado Corp DevCo”) was not fair or reasonable in the long term. 


It is important to note this negotiation happened in 2008 at the start of the Great Recession. This provided a dire frame of reference for both landlord and tenant. 


The lease had been signed six months prior with a different landlord who two months later went out of business and sold the property and leases to a new owner. This meant the new landlord was not privy to or bound by any verbal agreements the old landlord and current tenants made. 


The event triggering the negotiation was the new landlord closing off parking in the complex and reducing the spots available to both tenants and their clients. This change, which the tenant had a verbal understanding with the previous landlord would not happen, spurred the tenant to review her lease. When the tenant started poking at the terms of the lease and discussing with other business owners, she realized her rent was 20\% above market average\cite{Rents}, facilities were in worse condition than expected, and she had not received any up front money for tenant improvements, as is typical for new leases.\cite{Interview}


\section{Key players}
\subsection{Tenant}

One side of the negotiation is a small business owner who has just started her own restaurant. Financially constrained with money and income barely covering expenses, there was minimal profit available for foreseeable repairs. She is six months into a five-year lease and recently learned she is paying significantly higher rent per square foot than other tenants in the same area. She also felt slighted because the property was in worse condition than originally considered and the tenant did not receive money that tenants of this type typically receive when starting a new lease. The tenant had also recently seen a reduction in available parking due to new policies of the landlord.


The tenant, being an inexperienced negotiator and business person primarily focused on getting some money from the landlord for repairs and reducing rent to a more reasonable level. 

\textbf{Target:} \$1000/mo reduction in rent (value over 4.5 years is \$54,000), \$30,000 for repairs to facility.

\textbf{BATNA:} Because the tenant had already signed the lease and were in operation, the tenant’s best alternative to the negotiations was the status quo with the risk of the additional expenses driving them out of business. 

\textbf{Reservation Point:} As the BATNA is the status quo, the Reservation point would be no change to rent and no money for repairs. Mentally the tenant felt any deal which granted \$15,000 in some form would be a win and planned to continue pushing the landlord until reaching that amount.

\subsection{Landlord}
The other side of this negotiation is the landlord. The local landlord represents the business interests of a three member investment group operating in multiple states. The landlord is free to make operational decisions, but consults with his partners for non-standard decisions which impact their mutual investments. The landlord had recently purchased the property from the previous owner when that owner had been forced into bankruptcy due to the recession of 2008. The property has one anchor business and seven additional spaces, one of which is presently vacant. 

Because of the time that this negotiation was happening, the landlord was likely under a fair amount of pressure to keep tenants, as the overall economy had recently taken a significant downturn and retail establishments were closing in substantial numbers. This was balanced by the fact that the property still needed to be returning value to the owners and so his ability to reduce rents was limited.

\textbf{Target:} Maintain the lease with no changes.

\textbf{BATNA:} The landlord’s BATNA was to let the tenant fail and try to replace them with a tenant who would willingly pay the current rent. This option would likely require that the landlord provide a tenant finish allowance, as well as pay realtor fees to acquire a new tenant\cite{Fundera}.

\textbf{Reservation Price:} \$103,000. This number is calculated based upon the following estimates:
\begin{itemize}
\item \$18,000: Real estate agent commission of 6\% on value of five-year lease of \$300,000 \cite{BrokerRates}
\item \$60,000: Tenant Finish Allowance
\item \$15,000: Estimated 3 month vacancy 
\end{itemize}

\section{Analysis of the issues}
\subsection{Monthly Rent}
\subsubsection{Tenant:}
Under the current terms of the lease the tenant is obligated to pay \$5,000 per month; however, given market economics and current business health, it is in the interest of the tenant to procure more favorable rent terms in order to continue operating her restaurant. Specifically, the tenant desires to pay \$4,000 per month and expresses this proposal in a manner that aligns with the landlord’s underlying needs of maintaining occupancy of his leasable spaces. 


\subsubsection{Landlord:}
Although the landlord is under no obligation to reduce the rent, it may be in thier intrests to consider it due to the onset of the Great Recession. The tradeoff of earning 20\% above market rent by exerting legal power may yield unfavorable results for the landlord. For example, the Great Recession creates a probability of no longer earning said rent given tenant insolvency and the landlord may ultimately earn no monthly rental income\cite{DallasFed}. Given this, securing an amenable rent amount aligns both parties’ interest, maintaining an ongoing relationship whereby the restaurant is solvent and the landlord has paying tenants going forward.


\subsection{Parking Access}
\subsubsection{Tenant:}
 Under her prior lease, the tenant had verbal consent from the landlord to access parking for both direct and customer usage. The new landlord has begun restricting access to parking.  This is issue of indeterminate financial value for the both the tenant and landlord; however, it is a catalyst for exploring and understanding other lease terms that may be re-negotiated in order for the tenant to continue leasing the space.  The tenant will primarily explore these terms via the lense of mutual interests and rights (norms and expectations) as a tenant.

\subsubsection{Landlord:}
Although parking access as a term was not re-negotiated, it didn’t present as a rights issue for the landlord.  More specifically, under the terms of the contract the landlord can at his discretion use parking access as he sees fit.  There is no requirement for the landlord to accommodate parking requests by the tenant. Furthermore, the landlord is now contractually obligated to give those spots to the new anchor tenant, and can not reasonably renegotiate them back for a minor tenant.

\subsection{Property Improvements}
\subsubsection{Tenant:}
The issue of property improvements had additional sub-issues, making it somewhat complex. Specifically, the property currently has HVAC issues and damaged awnings around the front public outdoor space. The contract, originally signed with the original landlord, makes no financial commitment to provide the tenant resources to make in-kind improvements to the space. However, leases almost universally offer this for commercial properties. The tenant feels slighted by this and is requesting 2 months rent free in lieu of the customary tenant improvement allowance which normally would have been granted at the start of the lease. In addition to appealing to norms of the industry for this assistance, the tenant emphasized the opportunity to make construction and cosmetic improvements as an investment and asset for which the landlord would also benefit, improving curb-appeal that will attract future potential tenants to the currently vacant adjacent spaces.

\subsubsection{Landlord:}
 The asset’s attractiveness, in this case the quality of construction and external quality of commercial storefronts, is the key component from which the landlord drive’s future business and limits tenant vacancy.  As a result, the landlord has aligned interests in improving the space as a means to create a compelling commercial space to attract future tenants.  In explicit negotiating terms, this model where both the tenant and landlord understand their aligned interests, allows them to ultimately expand the pie by agreement to mutually beneficial terms. For the landlord some improvements such as HVAC repair are specifically beneficial to the tenant only, and so he will be less ammenable to paying for these versus exterior upgrades.

\subsection{Property Vacancy pending possible business closure}
\subsubsection{Tenant:}
This issue is not directly negotiable by the tenant but it is conceptually a theme the tenant can use to communicate the reasoning behind her proposed reduction in monthly rent. The concept of tenant vacancy is used by Boulder Bagels to highlight where both tenant and landlord interests align as a retail space with multiple empty locations will be more difficult to fill and is less attractive to future tenants. Additionally, a fully tennanted location leads to increased foot traffic and business for all tenants.

\subsubsection{Landlord:}
The landlord is optimizing for economic capacity and limited vacancy within its commercial properties. This results in the landlord generally agreeing to business terms that will lead to reduced vacancy. He is receptive to proposed business terms that ultimately lead to maximizing his long-term income in terms of both this property, and adjacent leases. Included in this strategy is his evaluation of the risk of long term vacancy and its impact on rental rates for the overall property.

\subsection{Economy Health}
\subsubsection{Tenant:}
This variable is the crux of why the tenant has approached the landlord to restructure the terms of the lease. With the onset of the Great Recession, both customers and businesses are facing troubling economic climates in 2008- consumers are spending less, commercial real estate markets are unpredictable, and the ability for businesses to maintain solvency is greatly reduced. Given the dire climate, the tenant is focused on highlighting a reduced rent payment as means to continue operating as a critical outcome.

\subsubsection{Landlord:}
Similar to the tenant, the landlord is currently facing micro-economic pressure to maintain enough tenants who can dependably pay rent. This is critical so the landlord can then pay his own ground lease landlord. Upon receiving the tenant’s request for a reduction in monthly rent it becomes immediately clear that both entities are focused on achieving a result that allows both to continue operating throughout 2008 and beyond. The concepts of power or rights, reflected in the landlord’s legal right to decline the tenant’s proposal, are generally disregarded because both power or rights does not yield the most attractive result for the landlord; a dependable and paying tenant.

\section{Distributive vs. Integrative}
The issues in this case are largely one sided with the tenant having a number of problems they need solved, and with little leverage to do so. The landlord is in the position of simply giving up money to accommodate the tenant’s request. That said, there are some external effects like the economic climate of the local Boulder economy. With the pending onset of the Great Recession, the landlord is optimizing his business model to ensure consistent and steady cash flow from tenants. As a result the negotiation starts out as a distributive negotiation. As a means to advance her interests and procure amenable terms, Boulder Bagels attempts to make it more integrative by raising additional issues such as that of property improvements. Exterior improvements for the building will present a better appearance for finding a tenant for the adjacent space and thereby being of benefit to both the landlord and tenant in the negotiation. A fully tenanted development ensures greater foot traffic, which is important for a new restaurant in finding new customers. A reduced rent to ensure long term tenancy likewise has the potential to benefit both landlord and tenant.


The Tenant knows this is largely a distributive discussion, but she also recognizes she has little leverage and needs to try to find some integrative solutions. As the tenant implicitly drives the negotiation to one that is integrative; the landlord responds in kind by supporting a similar negotiation model. For example, the landlord introduces other issues to discuss, a key signal of an integrative negotiation. Specifically the landlord knows that preserving and supporting tenants is critical. Thus the landlord provides other forms of financial support like subsidizing direct mailers as a means to help the tenant acquire more customers, and the landlord offers this conceptually rather than explicitly reducing the monthly rent per the tenant’s wishes. Additionally the landlord re-visited the terms of the lease to trade-off the short term demands of the tenant requiring reduced lease terms for securing longer-term tenant occupancy. By exploring different financial and commercial terms that align both parties interests, the tenant and landlord were able to agree to terms that provide the tenant some financial relief while giving the landlord the confidence of a dependable and rent-paying tenant.

\begin{center}
\begin{tabular}{ c c c c }
  Issue & Proposed by &  Original Negotiation Term & Integrated into Negotiation? \\ 
  Monthly Rent Payment & Tenant & Yes & Yes \\  
  Parking Access & Tenant & No & No\\
  Direct Mail Subsidy & Landlord & Yes & Yes \\
  Property Improvements & Tenant & Yes & Yes \\
  Lease Terms & Landlord & No & Yes
\end{tabular}
\end{center}

\section{Analysis of the context}
In understanding the context of this landlord-tenant dispute, completing a situation assessment is required to effectively understand how each party approached, perceived, and prepared their negotiation.  Below represents a selection of relevant questions that frame the terms of the negotiation:


\textit{Is the negotiation single-shot, repetitive or long-term?}

This dispute is over the ability to sustainably pay rent by the tenant; as a result it is a single negotiation.

\textit{Are scarce resources, ideologies, or both involved?}

This dispute centers around a consensus conflict rather than scarce resource competition; therefore ideologies and interests regarding what is best for the aggregate parties is present. Consequently, the interest of ensuring tenant affordability and limited property vacancy lead to both the tenant and landlord in good faith working to resolve the conflict.

\textit{Is the negotiation of necessity or opportunity?}

This negotiation is of necessity. The tenant is paying substantially above market rates for a leased space and does not have business cash flow for continuing to support the rent payment. If the landlord declines to negotiate, he is automatically placing the tenant in a position to which the tenant contends may lead to bankruptcy. That is likely to be a much worse outcome for the landlord than negotiating.

\textit{Is the negotiation a dispute or exchange?}

Governed by the lease contract, the negotiation is an exchange to revisit business terms of the contract, such as payment terms and monthly rent amount.

\textit{Are there linkage effects?}

Yes, the terms at which the landlord agrees to the tenant’s requests will inform future tenant rates and obligations. The outcome of this negotiation has implications for the future relationship between tenant and landlord. If the tenant is unsatisfied with the eventual outcome, it could lead to future negotiations to redress the same issues.

\textit{Is agreement required?}

Yes, for the tenant. If the tenant cannot come to agreement with the landlord her business may need to close. For the landlord no, if an agreement is not made the landlord can find other tenants to occupy the space.

\textit{Is it legal to negotiate?}

Yes, it is legal to negotiate as the lease is a voluntary agreement, and any alterations to the lease will be addendums to that same private legal document. There are no other legal constraints in this negotiation.

\textit{Are there time-constraints or other time-related costs?}

Yes, with each passing month the tenant’s financial health becomes more strained and therefore the tenant incurs additional costs without resolution.

\textit{Do negotiators communicate explicitly or implicitly?}

The negotiation is explicit with both the tenant and landlord engaging directly with each other.

\textit{Is there a power differential between the parties?}

Yes, the landlord has the legal contract as the current operating terms of the lease; the tenant is bound by these terms and simply is looking for a restructuring to ensure the ability to continue operations and make lease payments. The tenant has no power except to withhold rent payments and force expensive legal proceedings.

\textit{Is precedent important?}

Yes, future tenants will have a reference for market rates in the event the landlord accepts the tenant’s terms.  Additionally, precedent will be set that the landlord is amenable to working with tenants to ensure the best interests of all parties are considered. If the landlord concedes on rate re-negotiation in this case, there would be a potential precedent for other lessee’s to attempt to re-negotiate their lease terms as well.

\section{Analysis of the tactics and their outcomes}
\subsection{Interests, Rights, and Power:}
This landlord-tenant negotiation is viewed through the approach each party has taken in arguing their case: interests, rights, and power. In analyzing each phase of the negotiation one can see the choices made by each party to emphasis which approach they felt would yield the best outcome. 

Because the tenant was an inexperienced negotiator she made a few mistakes in the negotiation but also applied some techniques that are consistent with good negotiation tactics. Broadly speaking two different sets of tactics need to be used in this negotiation. For the issues that are distributive, the parties must each try to get as much of the “pie” as possible, and while discussing integrative issues they must work together. There is a third type of issue, those that are compatible, but these issues should quickly be resolved and set aside if found and are unknown if the parties don’t discuss them. 


\subsection{Round 1}
In the first round of the negotiation, the tenant approached the landlord with an initial proposal to reduce rent by 25\% to \$4,000 per month along with forgoing rent payments for the next two months to free up cash for facility repairs. The tenant justified this request by referencing the higher than average rent currently paid, the landlord’s closing of previously available parking space, the poor state of repair of some aspects of the facility, and the lack of any initial tenant improvement allowance to bring the facility up to good condition. The tenant stated that due to the burdensome rent, the tenant was struggling to stay in business.. 


The landlord responded that he was unwilling to renegotiate an established rental agreement, and explained that he had his own land lease payments to make. He expressed concerns that if the tenant could not be profitable, it may be better to fail sooner rather than later, so that he would minimize missed rent payments. However, the landlord acknowledged a willingness to help in an unspecified capacity after discussing the situation with his business partners. Additionally, he expressed his personal happiness with the business, and for purely personal reasons would like to see it succeed simply so he could continue to enjoy the products.

\subsubsection{Tenant Assessment:}
Here, the tenant opened the negotiation by making the first offer with an appeal to norms of fairness regarding the cost of rent, setting an important anchor point for the negotiation. The value of the proposal at \$64,000 was both optimistic and realistic based upon the tenant’s understanding of what a new tenant may be offered. The tenant gave further support to the request by identifying specific issues beyond the rent payments. In addition to an appeal to norms, the tenant has implied that if an agreement is not reached, that the tenant may have to close and cease paying rent through bankruptcy. 

\subsubsection{Landlord Assessment:}
The landlord did well by not rushing into a premature concession in response to the tenant’s proposal. He instead vocalized valid concerns to the initial request and took extra time to prepare a counter-offer. For the landlord, the request to lower the tenant’s monthly rent is a concession he can easily avoid by referring back to the contractual agreement as a non-negotiable topic. However, the landlord’s decision to continue the negotiation is wisely shifted toward coming to a solution where the tenant’s business will improve and thus result in decreased risk of missed rental payments in the future. 

\subsection{Round 2}
In a second round of negotiations, the landlord returned with a counter-proposal where he would pay for \$8,000 worth of direct mailers to help increase the tenant’s business. The landlord repeated that the monthly lease payments were not negotiable. 

The tenant responded by noting that while the offer could eventually help the business, the counter proposal did not address the immediate needs for facility repairs nor did it address the basic excessiveness of the rent. The tenant countered by requesting direct assistance with maintenance repairs of the building’s facilities, repeating the fact that the tenant never received the initial tenant improvement allowance traditionally provided. The tenant brought up concerns regarding parking availability. The tenant finished with the request to cancel the next 2 months of rental payments in addition to the offered assistance with the mailers.

\subsubsection{Landlord Assessment:}
The landlord returned to the negotiation table with a counteroffer, immediately re-anchoring the conversation and bringing another issue into the negotiation. This move by the landlord shifted the negotiation from a pie-slicing to a pie-expanding situation; one where both the landlord and the tenant could capitalize on opportunities for victories. While it may not be obvious how this benefits the landlord directly, it can be inferred that the landlord views this type of investment in the Tenant's long term success as giving the landlord greater assurance of long term rent payments. The landlord also declared a commitment to invest in a long-term relationship with the counter offer proposal. The value of this offer, \$8000, was a strong rebuttal to the initial request of \$64,000 requested by the tenant.

\subsubsection{Tenant Assessment:}
The tenant made an attempt to re-anchor in return to the landlord’s counter offer with a separate counter, bringing in more facts to support the revised proposal. The tenant’s negotiation strategy is focused on pie slicing tactics, where the offers and counter offers made focus on values added to the tenant without any obvious efforts exerted to find aligned goals or value for the landlord. While the tenant fares well in maintaining consistency, simplicity and effectiveness with the offers proposed, the tenant’s negotiation relies on appeals to standards of fairness. It lacks sufficient effort in searching for additional common ground which is needed to come to an optimal agreement.

\subsection{Round 3}
In a third and final round of negotiations, the landlord provided another counter proposal, offering a concession that if the tenant renew the lease for 5 full years, as opposed to the current four and a half years remaining on the current contract, the landlord would in turn defer payment of the next 2 months of rent to the end of the renewed 5-year lease. If signed, the landlord would follow through with his initial counter proposal to pay for direct mailers to promote the tenant’s restaurant business, and the landlord would additionally agree to send out a repair crew in order to assess the internal repairs needed and have them offer the tenant preferential pricing. 


After assessing this second counter proposal, the tenant stated a willingness to agree if the landlord also replaced the awnings to match the neighboring facility awnings, to which the landlord agreed, resulting in a mutual agreement between the tenant and landlord.

\subsubsection{Landlord Assessment:}
The landlord again provided a counter proposal with a concession positioning the negotiation toward a long-term relationship through a value-adding package deal. This time, the landlord conceded to the tenant’s request regarding payment over the next two months, appealing to the tenant’s voiced interests even though a power-approach could have been used since facility repairs contractually fell under the responsibility of the tenant. Ultimately, the landlord’s efforts to leverage an integrative negotiation approach allowed the landlord to land an agreement that found compromising alignments between the tenant’s interests as well as the landlord’s. This final round resulted in the landlord paying nearly \$16,000 for flyers, building repairs and improvements, while deferring \$10,000 in rent payment for five years.

\subsubsection{Tenant Assessment:}
The tenant completed the negotiation with one final counter offer to which the landlord agreed. Even though the landlord did not concede to any changes regarding the initial issue addressed, i.e. monthly rent, the tenant consistently negotiated for exclusive, self-interested offers through distributive strategies from a rights-based approach. 

\subsection{Compatible issues}
The issues of repair to the unit and it’s amenities are potentially compatible between the landlord and the tenant, but they quickly become integrative or even distributive when discussing who pays for the repairs. External repairs and upgrades to the tenants unit will primarily benefit the tenant. The external appearance of the awnings is a reflection upon the entire development, and thus while primarily benefiting the tenant, there are some benefits to the landlord in terms of presentation of the facility for existing and future tenants. 

\subsection{Integrative issues}
The first thing each side of the negotiation should have done when starting to discuss the integrative issues was try to take the perspective of the other party. This would hopefully allow them to see the issues the other party cares a lot about and they themselves do not care about. By exploring issues openly, each side would have a better chance of identifying common interests. This largely was not done, with each side making assuptions as to what the other side wanted and proposing offers based upon those assumptions. The tenant primarily viewed the landlord as wanting to manitain a paying tenant, and so from the tenant's perspective that was the only issue brought into the discussion. The landlord did a good job of understanding the tenant's longterm needs, but chose to delay addressing the tenants immediate financial concerns until later in the negotiation. This could have been a tactic, or a misunderstanding of the immediate need.

\subsection{Distributive issues}
When it came to the distributive issues the tenant, could have done a few things better. She approached the negotiations logically and unemotionally, but without the knowledge on how to prepare for the negotiation, and the approach which would maximize her share in the end. These are detailed below.  

\subsubsection{Places to improve - tenant}
The tenant failed to start the conversation in terms of values instead of numbers. By starting with values they could have more easily figured out what the landlord cared about and gotten both parties into a better mental state for negotiations. 


The tenant also didn’t plan the magnitude and timing of their concessions. By planning concessions the tenant would have been able to take advantage of  psychological traits, notably by making many small concessions they could improve the apparent value of the concessions. They also needed to label concessions they made and ask for reciprocity. This demand for reciprocity occured only during the final stage of negotiation, when the tenant agreed contingent upon new awnings. Other opportunities could have been taken advantage of prior to this instance.


Finally the tenant should have done more research into the landlord's reservation point and BATNA in order to make a better opening offer. The Tenant felt the opening was good, but in fact could have been higher given the high BATNA the landlord would have had. A caveat however - this assumes two logical parties. There are egos associated with this negotiation, and it’s possible that the landlord would have outright rejected a higher offer rather than allow his pride to be hurt by giving away money, as he may see it.

\subsubsection{Places the tenant did well}
The tenant did a number of things well in this negotiation. They did not reveal their reservation point and they set a high target point and initial offer. By making the first offer the tenant set the anchor point for the negotiation - this was to their advantage and by having high aspirations they put themselves into a good mental state and set the stage for what their expectations were. By using other rents in the same area the tenant was able to appeal to their view of fairness.

\subsubsection{Landlord mostly did well}
The landlord recognizes that however integrative the discussion, in the end he will be paying out of pocket in some capacity. Becuase of this, he attempts to make the discussion as integrative as possible, while holding firm on the distributive portions of the negotiation. He does not concede rent amounts, and made sure that half of the money was for improvements that could conceivably benefit him. The only criticism for the landlord may be that he should ensure to label his concessions. In this negotiation especially, the counterparty may not properly recognize the concession without it being made clear.


\section{Analysis of other possible additional relevant topics}
This negotiation presents itself as a single activity between a tenant and a landlord. However there are possibilities that in the future these two parties will meet again. The landlord has multiple commercial properties, as well as connections in the industry throughout the region. The tenant is a budding entrepreneur who has aspirations of opening multiple locations. As such both parties need to conclude this single-shot negotiation in a way such that they are positioned well for future negotiations with each other, or participants who may be aware of the outcome of this negotiation.

\section{Analysis of conclusions}
Re-negotiating an existing lease is inherently confrontational. Both parties are assumed to have agreed to the lease in good faith, yet the tenant feels the need to return to the bargaining table after the agreement is made. Therefore, it is interesting to note that both the Tenant, and especially the landlord, elected to found a lasting, long-term relationship and ensure both parties exited the negotiation amicably. The landlord was under no obligation to acquiesce to their tenant’s requests, and yet at each step of the negotiation, the landlord would guide the negotiation away from reducing rent payments - a strictly distributive position - towards a mutually beneficial offer. Given that the landlord at any point could have resorted to a power position if needed, this shows a level of negotiating acumen to instead direct the negotiations back to the issues and growing the pie.


As demonstrated, the tenant approached the negotiations from primarily a distributive position which hindered her opportunity to maximize the full potential of the negotiation. Her primary concern was freeing up money for operations and repairs. However, based on our analysis, she could have approached the negotiation from a more holistic perspective and increased the overall value of the “pie”. The landlord would obviously be inclined to reject outright requests for reduced rent, so it is important for the tenant to approach negotiations with a plan to identify and introduce mutually beneficial points of discussion.


The conclusion of the negotiation left all parties satisfied. The landlord agreed to pay for flyers, which would only benefit the business, as well as exterior improvements which would benefit both parties. The delay in 2 months rent primarily benefited the tenant and at minimal cost to the landlord. Compared to the alternative of locating a new tenant, this could be considered an enormous win. The tenant was happy as she deferred payment of rent for the next two months and was able to generate new business. The dollar value was far less than the target she set, but far greater than she was legally entitled per her lease. In the end, despite any errors she may have made in approaching the negotiation, she was right: It never hurts to ask.

\bibliographystyle{apacite}
\bibliography{rent_re-negotiation_analysis}

 \end{document}
